<?php

App::uses('Folder', 'Utility');

class Atmodel extends AppModel
{
    public $belongsTo = array(
        'User' => array(
            'className' => 'User',
            'foreignKey' => 'user_id'
        ),
        'Category' => array(
            'className' => 'Category',
            'foreignKey' => 'category_id',
            'dependent' => true
        )
    );

    public $hasMany = array(
        'File' => array(
            'className' => 'File',
            'foreignKey' => 'atmodel_id',
            'dependent' => true,
            'conditions' => array(
                'File.type' => 'files',
            )
        ),
        'Image' => array(
            'className' => 'File',
            'foreignKey' => 'atmodel_id',
            'dependent' => true,
            'conditions' => array(
                'Image.type' => 'img',
            )
        )
    );

    public function afterDelete()
    {
        $folder = new Folder(WWW_ROOT . 'files/' . $this->data['Atmodel']['slug'] . '/');
        if ($folder->delete()) {
            return true;
        }
        return false;
    }

    private function checkFilesDir($data)
    {
        if (!file_exists($this->filesDir('files', $data)) && !is_dir($this->filesDir('files', $data))) {
            if (!mkdir($this->filesDir('files', $data))) throw new Exception(__('Could not create file directory'));
        }
        if (!file_exists($this->filesDir('img/uploads', $data)) && !is_dir($this->filesDir('img/uploads', $data))) {
            if (!mkdir($this->filesDir('img/uploads', $data))) throw new Exception(__('Could not create image directory'));
        }
    }

    public function beforeSave($options = array())
    {
        $this->data['Atmodel']['user_id'] = AuthComponent::user('id');
        $this->data['Atmodel']['created_at'] = date('Y-m-d H:i:s');
    }
}
