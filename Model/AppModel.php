<?php
/**
 * Application model for Cake.
 *
 * This file is application-wide model file. You can put all
 * application-wide model-related methods here.
 *
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Model
 * @since         CakePHP(tm) v 0.2.9
 */

App::uses('Model', 'Model');
App::uses('Folder', 'Utility');
App::uses('File', 'Utility');

/**
 * Application model for Cake.
 *
 * Add your application-wide methods in the class below, your models
 * will inherit them.
 *
 * @package       app.Model
 */
class AppModel extends Model
{

    /**
     * Converts bytes into human readable file size.
     *
     * @param string $bytes
     * @return string human readable file size (2,87 Мб)
     * @author Mogilev Arseny
     */
    public function FileSizeConvert($bytes)
    {
        $bytes = floatval($bytes);
        $arBytes = array(
            0 => array(
                "UNIT" => "TB",
                "VALUE" => pow(1024, 4)
            ),
            1 => array(
                "UNIT" => "GB",
                "VALUE" => pow(1024, 3)
            ),
            2 => array(
                "UNIT" => "MB",
                "VALUE" => pow(1024, 2)
            ),
            3 => array(
                "UNIT" => "KB",
                "VALUE" => 1024
            ),
            4 => array(
                "UNIT" => "B",
                "VALUE" => 1
            ),
        );

        foreach ($arBytes as $arItem) {
            if ($bytes >= $arItem["VALUE"]) {
                $result = $bytes / $arItem["VALUE"];
                $result = str_replace(".", ",", strval(round($result, 2)))." ".$arItem["UNIT"];
                break;
            }
        }
        return $result;
    }

    protected function filesDir($what_is, $directory)
    {
        return new Folder(WWW_ROOT . $what_is . ($directory ? DS . $directory . DS : DS));
    }

    private function certifyDirectoryExists($directory, $path = WWW_ROOT)
    {
        if (count($directory) > 0) {
            $newPath = $path . DS . $directory[0];
            if (!file_exists($newPath)) mkdir($newPath);

            array_shift($directory);
            $this->certifyDirectoryExists($directory, $newPath);
        }

    }

    public function uploadFile(Array $file, $what_is = 'files', $directory = false, $uniqid = false)
    {
        $dirArray = split($directory, '/');
        if (empty($dirArray)) {
            $newDirectory = [$directory];
        } else {
            $newDirectory = $dirArray;
        }
        $this->certifyDirectoryExists($newDirectory, WWW_ROOT . DS . $what_is);
        $filename = ($uniqid) ? uniqid() . '-' . $file['name'] : $file['name'];
        if (move_uploaded_file($file['tmp_name'], $this->filesDir($what_is, $directory)->path . $filename))
            return $filename;
        else throw new Exception("Error uploading file (" . $what_is . ")");

    }
}
