<?php

class Category extends AppModel
{
    public $hasMany = array(
    	'Atmodel' => array(
            'className' => 'Atmodel',
            'foreignKey' => 'category_id',
            'dependent' => true,
        )
    );
}
