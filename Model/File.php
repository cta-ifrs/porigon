<?php

class File extends AppModel
{
    public $belongsTo = array('Atmodel');

    public $useTable = 'files';

    public function beforeDelete($cascade = true)
    {
        $data = $this->findById($this->id);
        if (unlink(WWW_ROOT . 'files/' . $data['Atmodel']['slug'] . '/' . $data['File']['name'])) {
            return true;
        }
        return false;
    }

    public function beforeSave($options = array())
    {
        // If comes from edition or addition of at models
        preg_match('/(?:.*)atmodels\/(edit|add|alt)\/(?:.*)/', $_SERVER['HTTP_REFERER'], $regex_output);
        if (empty($regex_output) || $regex_output[1] != 'alt') {
            $model = (isset($this->data['File'])) ? 'File' : 'Image';
            $atmodel = $this->Atmodel->findById($this->data[$model]['atmodel_id']);

            $this->data[$model]['type'] = (isset($this->data['File'])) ? 'files' : 'img';
            $this->uploadFile($this->data[$model],
                ($this->data[$model]['type'] == 'img') ? $this->data[$model]['type'] . DS . 'uploads' : $this->data[$model]['type'],
                $atmodel['Atmodel']['slug'],
            false);
        }
    }

    public function saveAlts($data)
    {
        foreach ($data['Image'] as $id => $file) {
            $this->clear();
            $this->id = $id;
            $this->set('alt', $file['alt']);
            if (!$this->save()) {
                throw new Exception(__('Was not possible to save alternative text'));
            }
        }
    }
}
