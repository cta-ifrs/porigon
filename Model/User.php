<?php


class User extends AppModel
{
    public $hasMany = array(
        'Atmodel' => array(
            'className' => 'Atmodel',
            'foreignKey' => 'user_id',
            'dependent' => true
        )
    );

    // public $validate = array(
    //     'name' => array(
    //         'required' => array(
    //             'rule' => array('notBlank'),
    //             'message' => __('A name is required')
    //         )
    //     ),
    //     'password' => array(
    //         'required' => array(
    //             'rule' => array('notBlank'),
    //             'message' => __('A password is required')
    //         )
    //     ),
    //     'email' => array(
    //         'required' => array(
    //             'rule' => array('notBlank'),
    //             'message' => __('An email is required')
    //         ),
    //         'email' => array(
    //             'rule' => array('email', false),
    //             'message' => __('Provide a valid email address')
    //         ),
    //         'unique' => array(
    //             'rule' => array('isUnique'),
    //             'message' => __('Email already in use')
    //         )
    //     )
    // );

    public function beforeSave($options = array())
    {
        $this->data[$this->alias]['photo'] = ($this->data[$this->alias]['photo']) ? $this->uploadFile($this->data[$this->alias]['photo'], 'img', 'profiles', true) : 'porigon.icon.png';
        if (isset($this->data[$this->alias]['password'])) {
            $this->data[$this->alias]['password'] = AuthComponent::password($this->data[$this->alias]['password']);
        }
        return true;
    }
}
