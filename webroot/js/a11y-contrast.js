$('#contrast-btn').keypress(function (event) {
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if (keycode == 13) {
        $(this).click();
    }
});

$('#contrast-btn').click(function () {
    var src = $(this).attr('src');
    $('body').toggleClass('contrast');
});
