$('.gallery-img').keypress(function (event) {
    var keycode = (event.keyCode ? event.keyCode : event.which);
    if (keycode == 13) {
        $(this).click();
    }
});

$('.gallery-img').click(function () {
    var src = $(this).attr('src');
    var alt = $(this).attr('alt');
    $('#atmodel-main-img').html("<span>" + alt + "</span>").attr('style', 'background: url(\'' + src + '\');');
});
