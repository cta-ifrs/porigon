# Porigon

Porigon é um repositório online de modelos 3D de Tecnologias Assitivas para impressão 3D.

## Instalação

- Requisitos:
    - PHP 5.6 ou superior (PHP 7 recomendável)
    - PDO habilitada para o PHP
    - Mcrypt habilitada para o PHP
    - mod_rewrite esteja habilitado no servidor web
    - Composer
    - Alguma engine de Banco de Dados suportada pelo Cake: MySQL, MariaDB, PostgreSQL, Microsoft SQL Server ou SQLite
- Copie o arquivo databse.php.default como databse.php e edite seu conteúdo de forma apropriada
- Certifique-se de que haja as permissões necessárias nos arquivos e diretórios para leitura pelo servidor web
- Certifique-se que o diretório app/temp e todos seus subdiretórios podem ser escritos tanto pelo servidor quanto pelo usuário
- Rode o comando composer install para instalar as dependências necessárias.
- Rode o arquivo porigon.sql para criar o Banco de Dados
---
Desenvolvido pelo [CTA - Centro Tecnológico de Acessibilidade do IFRS](http://cta.ifrs.edu.br)