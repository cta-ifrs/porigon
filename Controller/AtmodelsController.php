<?php

class AtmodelsController extends AppController
{
    public $components = array('Paginator');

    public $paginate = array(
        'limit' => 25,
        'order' => array(
            'Atmodel.created_at' => 'desc'
        )
    );

    public function index($cat = false)
    {
        $haveCategory = ($cat) ? array('category_id' => $cat) : null;
        $this->Paginator->settings = $this->paginate;
        $atmodels = $this->Paginator->paginate('Atmodel', $haveCategory);

        if ($atmodels != null) {
            $this->set('atmodels', $atmodels);
        } else {
            $this->Flash->warning(__("No Models in record"));
        }
    }

    public function add()
    {
        if ($this->request->is('post')) {
            try {
                $this->request->data['Atmodel']['slug'] = strtolower(Inflector::slug($this->request->data['Atmodel']['name'], '-'));
                $this->Atmodel->saveAll($this->request->data);
                $this->Flash->success(__("Model saved"));
                $this->redirect([
                    'action' => 'alt',
                    $this->request->data['Atmodel']['slug']
                ]);
            } catch (Exception $e) {
                $this->Flash->danger($e->getMessage());
                foreach ($this->Atmodel->validationErrors as $error) {
                    $this->Flash->danger($error[0]);
                }
            }
        }
        $this->set('categories', $this->Atmodel->Category->find('list'));
    }

    public function alt($slug = null)
    {
        $atmodel = $this->Atmodel->findBySlug($slug);

        if (!$atmodel) {
            throw new NotFoundException(_('AT Model does not exist'));
        }

        $this->set('atmodel', $atmodel);
        if ($this->request->is('post')) {
            try {
                $this->Atmodel->File->saveAlts($this->request->data);
                $this->Flash->success(__('Alternative text added with success'));
                $this->redirect(['action' => 'view', $slug]);
            } catch (Exception $e) {
                $this->Flash->danger($e);
            }
        }
    }

    public function delete($id)
    {
        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
        }
        if ($this->Atmodel->delete($id, true)) {
            $this->Flash->success(__('Atmodel deleted with success'));
            $this->redirect(array('action' => 'index'));
        }
    }

    public function edit($slug = null)
    {
        $atmodel = $this->Atmodel->findBySlug($slug);
        if (!$atmodel) {
            throw new NotFoundException(_('AT Model does not exist'));
        }

        $this->Atmodel->id = $atmodel['Atmodel']['id'];
        $this->set('atmodel_id', $atmodel['Atmodel']['id']);
        if ($this->request->is('get')) {
            $this->request->data = $atmodel;
            $this->set('categories', $this->Atmodel->Category->find('list'));
        } else {
            try {
                if ($this->checkFileUpdate()) {
                    $this->request->data['Atmodel']['id'] = $atmodel['Atmodel']['id'];
                    $this->Atmodel->saveAll($this->request->data);
                    $this->Flash->success(__('AT Model successfuly saved!'));
                    $redirect_action = (isset($this->request->data['Image'])) ? 'alt' : 'view';
                    $this->redirect(array('action' => $redirect_action, $slug));
                } else {
                    throw new Exception(__('File input is empty'));
                }
            } catch (Exception $e) {
                $this->Flash->danger($e->getMessage());
            }
        }
    }

    private function checkFileUpdate()
    {
        if (isset($this->request->data['File'])) {
            if (empty($this->request->data['File'])) {
                return false;
            } else {
                return true;
            }
        } elseif (isset($this->request->data['Image'])) {
            if (empty($this->request->data['Image'])) {
                return false;
            } else {
                return true;
            }
        }

        return true;
    }

    public function view($slug = null)
    {
        $atmodel = $this->Atmodel->findBySlug($slug);

        if (!$atmodel) {
            throw new NotFoundException(_('AT Model does not exist'));
        }

        $this->set('title', $atmodel['Atmodel']['name']);
        $this->set('atmodel', $atmodel);
    }
}
