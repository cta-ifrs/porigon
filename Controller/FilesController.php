<?php

class FilesController extends AppController
{
    public function delete($id)
    {
        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
        }
        if ($this->File->delete($id, true)) {
            $this->Flash->success(__('File deleted with success'));
            $this->redirect($this->referer());
        }
    }

    public function download($dir, $file)
    {
        $this->response->file(WWW_ROOT . 'files/' . $dir . '/' . $file, array('download' => true));

        return $this->response;
    }
}
