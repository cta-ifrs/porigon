<?php

class CategoriesController extends AppController
{
    public function index() {
        $categories = $this->Category->find('all');
        $this->set('categories', $categories);
    }

    public function view($id) {
        $this->redirect(array(
            'controller' => 'atmodels',
            'action' => 'index',
            $id
        ));
    }

    public function edit($id) {
        $category = $this->Category->findById($id);
        if (!$category) {
            throw new NotFoundException(_('Category does not exist'));
        }
        $this->Category->id = $category['Category']['id'];
        if ($this->request->is('get')) {
            $this->request->data = $category;
        } else {
            try {
                $this->Category->save($this->request->data);
            } catch (Exception $e) {
                $this->Flash->danger($e->getMessage());
                foreach ($this->Atmodel->validationErrors as $error) {
                    $this->Flash->danger($error[0]);
                }
            }
        }
    }

    public function delete($id) {
        if (!$this->request->is('post')) {
            throw new MethodNotAllowedException();
        }
        if ($this->Category->delete($id, true)) {
            $this->Flash->success(__('Category deleted with success'));
            $this->redirect(array('action' => 'index'));
        }
    }

    public function add() {
        if ($this->request->is('post')) {
            try {
                $this->Category->save($this->request->data);
                $this->redirect(['action' => 'index']);
            } catch (Exception $e) {
                $this->Flash->danger($e->getMessage());
                foreach ($this->Atmodel->validationErrors as $error) {
                    $this->Flash->danger($error[0]);
                }
            }
        }
    }
}
