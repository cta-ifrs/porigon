<div class="example-modal col-md-8 col-md-offset-2">
    <div class="modal modal-default">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h1><?php echo __('Add Category') ?></h1>
                </div>
                <div class="modal-body">
                    <?php echo $this->Form->create('Atmodels', array(
                        'inputDefaults' => array(
                            'div' => 'form-group',
                            'wrapInput' => false,
                            'class' => 'form-control'
                        ),
                    )); ?>
                    <fieldset>
                        <?php echo $this->Form->input('Category.name'); ?>
                    </fieldset>
                    <fieldset>
                        <?php echo $this->Form->submit(__d('cake', 'Submit'), array(
                            'class' => 'btn btn-success btn-lg'
                        )); ?>
                    </fieldset>
                    <?php echo $this->Form->end(); ?>
                </div>
            </div>
        </div>
    </div>
</div>
