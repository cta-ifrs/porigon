<div class="example-modal col-md-8 col-md-offset-2">
    <div class="modal modal-default">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h1>Categorias</h1>
                </div>
                <div class="modal-body">
                    <ul class="todo-list" style="font-size:2em">
                        <?php foreach ($categories as $key => $cat): ?>
                            <li><?php echo $this->Html->link($cat['Category']['name'], array(
                                'controller' => 'categories',
                                'action' => 'view',
                                $cat['Category']['id']
                            )) ?><small style="font-size:1.5rem" class="pull-right"><?php echo count($cat['Atmodel']) ?> models in record</small></li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>
