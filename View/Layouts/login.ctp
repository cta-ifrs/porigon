<!DOCTYPE html>
<html>
    <head>
        <?php echo $this->Html->charset(); ?>
        <title>
            <?php
            if (isset($title)) echo $title;
            else echo 'Porigon: ' . __($this->fetch('title'));
            ?>
        </title>
        <?php
            echo $this->Html->meta('icon');

            echo $this->Html->css('bootstrap.min.css');
            echo $this->Html->css('AdminLTE.min.css');
            echo $this->Html->css('skins/_all-skins.min.css');
            echo $this->Html->css('bootstrap3-wysihtml5.min.css');
            echo $this->Html->css('https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');
            echo $this->Html->css('https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css');
            echo $this->Html->css('app.css');

            echo $this->Html->script('jquery-2.2.3.min.js');
            echo $this->Html->script('bootstrap.min.js');
            echo $this->Html->script('bootstrap3-wysihtml5.all.min.js');
            echo $this->Html->script('app.min.js');

            echo $this->fetch('meta');
            echo $this->fetch('css');
            echo $this->fetch('script');
        ?>
    </head>
    <body class="hold-transition login-page skin-blue layout-top-nav">
        <div class="wrapper">
            <?php
            echo $this->element('a11yheader');
            echo $this->fetch('content');
            echo $this->Html->script('a11y-contrast.js');
            echo $this->Html->script('main.js');
            ?>
        </div>
    </body>
</html>
