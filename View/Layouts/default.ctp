<!DOCTYPE html>
<html>
    <head>
        <?php echo $this->Html->charset(); ?>
        <title><?php if (isset($title)) {
            echo $title;
        } else {
            echo 'Porigon: ' . __($this->fetch('title'));
        } ?></title>
        <?php
            echo $this->Html->meta('icon');

            echo $this->Html->css('bootstrap.min.css');
            echo $this->Html->css('AdminLTE.min.css');
            echo $this->Html->css('skins/_all-skins.min.css');
            echo $this->Html->css('bootstrap3-wysihtml5.min.css');
            echo $this->Html->css('https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css');
            echo $this->Html->css('https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css');
            echo $this->Html->css('app.css');

            echo $this->Html->script('jquery-2.2.3.min.js');
            echo $this->Html->script('bootstrap.min.js');
            echo $this->Html->script('bootstrap3-wysihtml5.all.min.js');
            echo $this->Html->script('app.min.js');

            echo $this->fetch('meta');
            echo $this->fetch('css');
            echo $this->fetch('script');
        ?>
    </head>
    <body class="skin-blue layout-top-nav">
        <div class="wrapper">
            <?php echo $this->element('a11yheader'); ?>
            <div class="content-wrapper">
                <div class="container">
                    <div class="content-header">
                        <div class="row">
                            <div class="col-md-3 col-sm-12">
                                <?php echo $this->Html->image('logo.png', array(
                                    'alt' => __('Porigon Logo'),
                                    'class' => 'index-logo',
                                    'url' => '/'
                                )); ?>
                            </div>
                            <div class="col-md-9">
                                <nav id="main-nav">
                                    <ul class="nav navbar-nav pull-right">
                                        <?php if (AuthComponent::user('id')): ?>
                                            <li><?php echo $this->Html->link(
                                                '<i class="fa fa-plus"></i>' . __('Add Model'),
                                                array('controller' => 'atmodels', 'action' => 'add'),
                                                array('escape' => false)
                                            ); ?></li>
                                        <?php endif; ?>
                                        <li><?php echo $this->Html->link(
                                            '<i class="fa fa-cube"></i>' . __('Categories'),
                                            array('controller' => 'categories', 'action' => 'index'),
                                            array('escape' => false)
                                        ); ?></li>
                                        <?php if (AuthComponent::user('id')): ?>
                                            <li><?php echo $this->Html->link(
                                                '<i class="fa fa-plus"></i>' . __('Add Category'),
                                                array('controller' => 'categories', 'action' => 'add'),
                                                array('escape' => false)
                                            ); ?></li>
                                        <?php endif; ?>
                                        <li><?php echo $this->Html->link(
                                            '<i class="fa fa-info"></i>' . __('About'),
                                            array('controller' => 'pages', 'action' => 'about'),
                                            array('escape' => false)
                                        ); ?></li>
                                        <?php if (AuthComponent::user('id')): ?>
                                            <li><?php echo $this->Html->link(
                                                '<i class="fa fa-user"></i>' . __('Profile'),
                                                array('controller' => 'users', 'action' => 'view', 'profile'),
                                                array('escape' => false)
                                            ); ?></li>
                                            <li><?php echo $this->Html->link(
                                                '<i class="fa fa-sign-out"></i>' . __('Logout'),
                                                array('controller' => 'users', 'action' => 'logout'),
                                                array('escape' => false)
                                            ); ?></li>
                                        <?php else: ?>
                                            <li><?php echo $this->Html->link(
                                                '<i class="fa fa-sign-in"></i>' . __('Login or Register'),
                                                array('controller' => 'users', 'action' => 'login'),
                                                array('escape' => false)
                                            ); ?></li>
                                        <?php endif; ?>
                                    </ul>
                                </nav>
                            </div>
                        </div>
                    </div>
                    <div class="content body" id="main-content">
                        <?php echo $this->Session->flash(); ?>
                        <?php echo $this->fetch('content'); ?>
                    </div>
                </div>
            </div>
            <footer class="main-footer">
                <p>
                    Desenvolvido pelo CTA - Centro Tecnológico de Acessibilidade do IFRS
                </p>
            </footer>
        </div>
        <?php
        echo $this->Html->script('a11y-gallery.js');
        echo $this->Html->script('a11y-contrast.js');
        echo $this->Html->script('main.js');
        ?>
    </body>
</html>
