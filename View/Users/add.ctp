<div class="register-box">
    <div class="register-logo">
        <!-- TODO: remover o img e por a imagem por css -->
        <?php echo $this->Html->link(
            $this->Html->image('logo.png', array('style' => 'width: 100%; margin-left: -25px;')),
            array('controller' => 'atmodels', 'action' => 'index'),
            array('escape' => false)
        ); ?>
    </div>
    <div class="register-box-body">
        <h1 class="login-box-msg"><?php echo __('New Account') ?></h1>
        <?php
        echo $this->Session->flash();
        echo $this->Form->create('User', array(
            'type' => 'file'
        ));
        echo $this->Form->input('name', array('class' => 'form-control'));
        echo $this->Form->input('email', array('class' => 'form-control'));
        echo $this->Form->input('password', array('class' => 'form-control'));
        echo $this->Form->input('photo', array(
            'type' => 'file'
        ));
        echo $this->Form->input('bio', array('class' => 'form-control', 'type' => 'textarea', 'label' => __('Short Bio') . ' (' . __('optional') . ')'));
        echo $this->Form->submit(__('Create'), array('class' => 'btn btn-primary btn-block btn-flat'));
        echo $this->Form->end();
        echo $this->Html->link(__('I already have an account'), array('controller' => 'users', 'action' => 'index'));
        ?>
    </div>
</div>
