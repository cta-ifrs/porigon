<section class="content-header">
    <h1><?php echo __('Edit Profile'); ?></h1>
</section>
<section class="content">
    <div class="box box-default">
        <div class="row box-body">
            <?php echo $this->Form->create('User', array(
                'inputDefaults' => array(
                    'div' => 'form-group',
                    'wrapInput' => false,
                    'class' => 'form-control'
                ),
                'type' => 'file'
            )); ?>
            <fieldset class="col-md-2 column">
                <?php echo $this->Html->image('profiles/' . $this->request->data['User']['photo'], array(
                    'style' => 'width: 100%; height: auto'
                )); ?>
            </fieldset>
            <fieldset class="col-md-4 column">
                <?php
                echo $this->Form->input('name');
                echo $this->Form->input('email');
                echo $this->Form->input('photo', array(
                    'type' => 'file',
                    'multiple',
                    'label' => __('Change Profile Photo')
                ));
                ?>
            </fieldset>
            <fieldset class="col-md-6 column">
                <?php
                echo $this->Form->input('bio', array(
                    'type' => 'textarea'
                ));
                ?>
            </fieldset>
            <fieldset class="col-md-12">
                <?php echo $this->Form->submit(__d('cake', 'Submit'), array(
                    'class' => 'btn btn-lg btn-success pull-right'
                )); ?>
            </fieldset>
            <?php echo $this->Form->end(); ?>
        </div>
    </div>
    <?php // echo $this->Form->postLink(__('Deactivate Account'), array('controller' => 'users', 'action' => 'delete', $this->request->data['User']['id']), array(
        // 'class' => 'btn btn-danger btn-sm'
    //)); ?>
    <!-- TODO: add isso posteriormente -->
</section>
