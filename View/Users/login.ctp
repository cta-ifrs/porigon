<div class="login-box">
    <div class="login-logo">
        <!-- TODO: remover o img e por a imagem por css -->
        <?php echo $this->Html->link(
            $this->Html->image('logo.png', array('style' => 'width: 100%; margin-left: -25px;')),
            array('controller' => 'atmodels', 'action' => 'index'),
            array('escape' => false)
        ); ?>
    </div>
    <div class="login-box-body">
        <h1 class="login-box-msg"><?php echo __('Login') ?></h1>
        <?php
        echo $this->Session->flash();
        echo $this->Form->create('User');
        echo $this->Form->input('email', array('class' => 'form-control'));
        echo $this->Form->input('password', array('class' => 'form-control'));
        echo $this->Form->submit(__('Login'), array('class' => 'btn btn-primary btn-block btn-flat'));
        echo $this->Form->end();
        echo $this->Html->link(__('I don\'t have an account'), array('controller' => 'users', 'action' => 'add'));
        ?>
    </div>
</div>
