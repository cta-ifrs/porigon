<section class="content-header">
    <h1><?php echo __('Profile of') . ' ' . $user['User']['name']; ?></h1>
</section>
<section class="content">
  <div class="row">
    <div class="col-md-3">
        <div class="box box-primary">
            <div class="box-body box-profile">
                <?php echo $this->Html->image('profiles/' . $user['User']['photo'], array(
                    'class' => 'profile-img img-responsive img-circle',
                    'alt' => __('Profile photo')
                )) ?>
                <h2><i class="fa fa-address-book-o"></i> <?php echo __('About me') ?></h2>
                <p><i class="fa fa-mail sr-only"><?php echo __('Email') ?>:</i> <?php echo $this->Html->link($user['User']['email'], 'mailto:' . $user['User']['email']); ?></p>
                <p><?php if ($user['User']['bio']) {
                    echo $user['User']['bio'];
                } else {
                    echo __('No bio provided');
                } ?></p>
            </div>
        </div>
    </div>
    <div class="col-md-9">
        <div class="box box-secondary">
            <div class="box-header with-border">
                <h2 class="box-title"><?php echo __('AT Models from this author'); ?></h2>
            </div>
            <div class="box-body">
                <ul class="atmodels-user-list">
                    <?php foreach ($user['Atmodel'] as $key => $atmodel): ?>
                        <li class="row user-model">
                            <span class="col-md-2 columns">
                                <?php echo $this->Html->image(
                                    'porigon.icon.png',
                                    array('width' => '100%', 'height' => 'auto')
                                ); ?>
                            </span>
                            <span class="columns">
                                <h3><?php echo $this->Html->link(
                                    $atmodel['name'],
                                    array('controller' => 'atmodels', 'action' => 'view', $atmodel['slug'])
                                ); ?></h3>
                                <ul class="menu nav navbar-nav atmodel-user">
                                    <li><i class="fa fa-cube"></i> Categoria: foo bar</li>
                                    <li><i class="fa fa-arrow-down"></i> Downloads: 2.3k</li>
                                    <li><i class="fa fa-file"></i> 3 arquivos</li>
                                </ul>
                                <!-- <a href="#"><i class="fa fa-plus-square-o"></i> show description</a> -->
                                <?php // echo $atmodel['description'] ?>
                                <!-- TODO: fazer isto, cumpadre -->
                            </span>
                        </li>
                    <?php endforeach; ?>
                </ul>
            </div>
        </div>
    </div>
  </div>
</section>
