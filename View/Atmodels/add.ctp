<div class="box box-default">
    <div class="box-header with-border">
        <h2><?php echo __('Add Model'); ?></h2>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Accusantium autem blanditiis dicta, dolore ducimus eaque earum facere facilis impedit, itaque non numquam odit placeat porro ratione suscipit totam vel voluptas?</p>
    </div>
    <?php echo $this->Form->create('Atmodels', array(
        'inputDefaults' => array(
            'div' => 'form-group',
            'wrapInput' => false,
            'class' => 'form-control'
        ),
        'type' => 'file'
    )); ?>
    <fieldset class="box-body">

        <fieldset class="col-md-6">
            <?php echo $this->Form->input('Atmodel.name'); ?>
        </fieldset>

        <fieldset class="col-md-6">
            <?php echo $this->Form->input('Atmodel.category_id'); ?>
        </fieldset>

        <fieldset class="col-md-6">
            <?php echo $this->Form->input('File. ', array(
                'type' => 'file',
                'multiple',
                'label' => __('Files')
            )); ?>
        </fieldset>
        <fieldset class="col-md-6">
            <?php echo $this->Form->input('Image. ', array(
                'type' => 'file',
                'multiple',
                'label' => __('Images')
            )); ?>
        </fieldset>

        <fieldset class="col-md-12">
            <?php echo $this->Form->input('Atmodel.description', array(
                'type' => 'textarea',
                'id' => 'description-editor'
            )); ?>
        </fieldset>



    </fieldset>
    <fieldset class="box-footer">
        <?php echo $this->Form->submit(__d('cake', 'Submit'), array(
            'class' => 'btn btn-success btn-lg'
        )); ?>
    </fieldset>
    <?php echo $this->Form->end(); ?>
    <script type="text/javascript">
        $('#description-editor').wysihtml5({
            toolbar: {
                "fa": true
            }
        });
    </script>
</div>
