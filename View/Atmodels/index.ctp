<?php if (isset($atmodels)): ?>
<div class="row">
    <div class="col-sm-12">
        <h2><?php echo __('AT Models'); ?></h2>
    </div>
    <?php foreach ($atmodels as $atmodel): ?>
    <div class="col-md-4">
        <!-- TODO: melhorar isso ai, o mais correto é utilizar o helper do Cake -->
        <a href="/atmodels/view/<?php echo $atmodel['Atmodel']['slug'] ?>">
            <div class="box box-default atmodel-widget">
                <div class="box-body atmodel-widget-img" style="background: url('<?php echo FULL_BASE_URL . '/img/uploads/' . $atmodel['Atmodel']['slug'] . '/' . $atmodel['Image'][0]['name']; ?>');">
                </div>
                <div class="box-footer">
                    <h2 class="box-title"><?php echo $atmodel['Atmodel']['name']; ?></h2>
                </div>
            </div>
        </a>
    </div>
    <?php endforeach; ?>
    <div class="col-sm-12 pager">
        <?php echo $this->Paginator->pager(array(
        	'prev' => __('Newer'),
        	'next' => __('Older')
        )); ?>
    </div>
</div>
<?php endif;
