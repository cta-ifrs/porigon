<div class="row">
    <div class="col-md-12">
        <h2><?php echo __('Add alternative text to images') ?></h2>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Deleniti esse hic ipsa, laboriosam magni modi molestiae mollitia obcaecati repudiandae ullam. Asperiores atque dolorem labore nobis quas quia, ratione totam ullam.</p>
        <?php echo $this->Form->create('File', [
            'inputDefaults' => [
                'div' => 'form-group',
                'wrapInput' => false,
                'class' => 'form-control'
            ]
        ]);
        foreach ($atmodel['Image'] as $img): ?>
            <fieldset class="box box-default">
                <fieldset class="box-header">
                    <label for="alt-<?php echo $img['id']; ?>" class="box-title"><?php echo __('File') . ': ' . $img['name']; ?></label>
                </fieldset>
                <fieldset class="box-body">
                    <fieldset class="row">
                        <fieldset class="col-md-3">
                            <?php $alt = (isset($img['alt']) && ($img['alt'] != '' || $img['alt'] != null)) ? $img['alt'] : __('Alt info not set for this image');
                            echo $this->Html->image('uploads/' . $atmodel['Atmodel']['slug'] . '/' . $img['name'], array(
                                'width' => '100%',
                                'height' => 'auto',
                                'alt' => $alt
                            )); ?>
                        </fieldset>
                        <fieldset class="col-md-9">
                            <?php echo $this->Form->input('Image.' . $img['id'] . '.alt', [
                                'id' => 'alt-' . $img['id'],
                                'type' => 'textarea',
                                'style' => 'width: 100%',
                                'label' => false,
                                'value' => $img['alt']
                            ]) ?>
                        </fieldset>
                    </fieldset>
                </fieldset>
            </fieldset>
        <?php endforeach; ?>

        <?php
        echo $this->Form->submit(__d('cake', 'Submit'), ['class' => 'btn btn-success btn-lg']);
        echo $this->Form->end();
        ?>
    </div>
</div>
