<div class="row">
    <div class="col-md-12">
        <h2 class="atmodel-title">
            <?php echo $atmodel['Atmodel']['name']; ?>
        </h2>
    </div>
    <div class="col-md-5">
        <div class="box box-default">
            <div class="box-body">
                <ul class="list-unstyled">
                    <li><?php echo __('Author') . ': ' . $this->Html->link(
                        $atmodel['User']['name'],
                        array('controller' => 'users', 'action' => 'view',
                        $atmodel['User']['id'])
                    ); ?></li>
                    <li><?php echo __('Category') . ': ' . $this->Html->link(
                        $atmodel['Category']['name'],
                        array('controller' => 'categories', 'action' => 'view',
                        $atmodel['Category']['id'])
                    ); ?></li>
                </ul>
            </div>
            <?php if (AuthComponent::user('id') != null &&
                ((AuthComponent::user('id') == $atmodel['Atmodel']['user_id']) || (bool) AuthComponent::user('is_admin'))): ?>
                <div class="btn-group btn-group-justified atmodel-options">
                    <?php
                    echo $this->Html->link(
                        __('Edit') . '<span class="sr-only">' . $atmodel['Atmodel']['name'] . '</span>',
                        '/atmodels/edit/' . $atmodel['Atmodel']['slug'],
                        array(
                            'class' => 'btn btn-primary',
                            'escape' => false
                        )
                    );

                    echo $this->Form->postLink(
                        __('Delete') . '<span class="sr-only">' . $atmodel['Atmodel']['name'] . '</span>',
                        '/atmodels/delete/' . $atmodel['Atmodel']['id'],
                        array(
                            'class' => 'btn btn-danger',
                            'escape' => false
                        )
                    );
                    ?>
                </div>
            <?php endif; ?>
        </div>
        <div class="box box-primary">
            <div class="box-header atmodel-widget-img" id="atmodel-main-img" style="background: url('<?php echo FULL_BASE_URL . '/img/uploads/' . $atmodel['Atmodel']['slug'] . '/' . $atmodel['Image'][0]['name']; ?>');">
            <?php if (!isset($atmodel['Image'][1])): ?>
                <span class="sr-only"><?php echo $atmodel['Image'][0]['alt']; ?></span>
            </div>
            <?php else: ?>
            </div>
            <div class="box-body">
                    <ul class="row">
                    <?php foreach ($atmodel['Image'] as $img): ?>
                        <li class="col-xs-3 gallery-img-container">
                            <?php echo $this->Html->image('uploads/' . $atmodel['Atmodel']['slug'] . '/' . $img['name'], array(
                                'class' => 'gallery-img',
                                'tabindex' => '0',
                                'alt' => $img['alt']
                            )) ?>
                        </li>
                    <?php endforeach; ?>
                    </ul>
                </div>
            <?php endif; ?>
        </div>

        <div class="box box-success">
            <div class="box-header with-border">
                <h2 class="box-title"><?php echo __('Files') ?></h2>
                <span class="pull-right"><a href="#">Download All (zip file)</a></span>
            </div>
            <div class="box-body">
                <?php foreach ($atmodel['File'] as $file): ?>
                    <?php $extension = pathinfo(WWW_ROOT . '/files/' . $atmodel['Atmodel']['slug'] . '/' . $file['name'])['extension']; ?>
                    <ul class="mailbox-attachments clearfix">
                        <li>
                            <span class="mailbox-attachment-icon"><i class="fa fa-file-pdf-o"></i></span>
                            <div class="mailbox-attachment-info">
                                <?php
                                    $file_complete = WWW_ROOT . 'files/' . $atmodel['Atmodel']['slug'] . '/' . $file['name'];
                                    echo $this->Html->link(
                                        $file['name'] . ' (' . $this->Data->formatBytes(filesize($file_complete)) . ')',
                                        array('controller' => 'files', 'action' => 'download', $atmodel['Atmodel']['slug'], $file['name']),
                                        array('class' => 'mailbox-attachment-name')
                                    ); ?>
                            </div>
                        </li>
                    </ul>
                <?php endforeach; ?>
            </div>
        </div>
    </div>
    <div class="col-md-7">
        <div class="box box-default">
            <div class="box-header with-border">
                <h2 class="box-title"><?php echo __('Description'); ?></h2>
            </div>
            <div class="box-body">
                <?php echo $atmodel['Atmodel']['description']; ?>
            </div>
        </div>
    </div>
</div>
