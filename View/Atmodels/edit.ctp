<div class="row">
    <div class="col-md-12">
        <h2>Editar Modelo de TA</h2>
    </div>
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header with-border">
                <h3><?php echo __('Information') ?></h3>
            </div>
            <?php echo $this->Form->create('Atmodels', array(
                'inputDefaults' => array(
                    'div' => 'form-group',
                    'wrapInput' => false,
                    'class' => 'form-control'
                ),
                'type' => 'file'
            )); ?>
            <fieldset class="box-body">
                <?php echo $this->Form->input('Atmodel.name'); ?>
                <?php echo $this->Form->input('Atmodel.category_id'); ?>
                <?php echo $this->Form->input('Atmodel.description', array(
                    'type' => 'textarea',
                    'id' => 'description-editor'
                )); ?>
            </fieldset>
            <fieldset class="box-footer">
                <?php echo $this->Form->submit(__('Update Information'), array(
                    'class' => 'btn btn-success'
                )); ?>
            </fieldset>
            <?php echo $this->Form->end(); ?>
            <script type="text/javascript">
                $('#description-editor').wysihtml5({
                    toolbar: {
                        "fa": true
                    }
                });
            </script>
        </div>
    </div>

    <div class="col-md-6">
        <div class="box box-success">
            <div class="box-header with-border">
                <h3><?php echo __('Images') ?></h3>
            </div>
            <?php echo $this->Form->create('Atmodel', array(
                'inputDefaults' => array(
                    'div' => 'form-group',
                    'wrapInput' => false,
                    'class' => 'form-control'
                ),
                'type' => 'file'
            )); ?>
            <fieldset class="box-body">
                <?php echo $this->Form->input('Image. ', array(
                    'type' => 'file',
                    'multiple',
                    'class' => '',
                    'required',
                    'label' => __('Add Images')
                )); ?>
            </fieldset>
            <fieldset class="box-footer">
                <?php echo $this->Form->submit(__('Update Images'), array(
                    'class' => 'btn btn-success'
                )); ?>
            </fieldset>
            <?php echo $this->Form->end(); ?>
        </div>
        <?php foreach ($this->request->data['Image'] as $img): ?>
            <div class="info-box" tabindex="0">
                <div class="info-box-icon img-edit" style="background: url('<?php echo FULL_BASE_URL . '/img/uploads/' . $this->request->data['Atmodel']['slug'] . '/' . $img['name']; ?>');">
                </div>
                <div class="info-box-content">
                    <div class="info-box-text"><h4><?php echo $img['name']; ?></h4></div>
                    <div class="info-box-text"><?php echo $this->Form->postLink(
                        __('Delete') . ' <span class="sr-only">' . $img['name'] . '</span>', '/files/delete/' . $img['id'],
                        array(
                            'confirm' => __('Are you sure?'),
                            'escape' => false
                        )
                    );?></div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
    <div class="col-md-6">
        <div class="box box-info">
            <div class="box-header with-border">
                <h3><?php echo __('Files') ?></h3>
            </div>
            <?php echo $this->Form->create('Atmodel', array(
                'inputDefaults' => array(
                    'div' => 'form-group',
                    'wrapInput' => false,
                    'class' => 'form-control'
                ),
                'type' => 'file'
            )); ?>
            <fieldset class="box-body">
                <?php echo $this->Form->input('File. ', array(
                    'type' => 'file',
                    'multiple',
                    'class' => '',
                    'label' => __('Add Files'),
                    'required',
                )); ?>
            </fieldset>
            <fieldset class="box-footer">
                <?php echo $this->Form->submit(__('Update Files'), array(
                    'class' => 'btn btn-success'
                )); ?>
            </fieldset>
            <?php echo $this->Form->end(); ?>
        </div>
        <?php foreach ($this->request->data['File'] as $file): ?>
            <div class="info-box" tabindex="0">
                <div class="info-box-icon">
                   <i class="fa fa-file-o"></i>
                </div>
                <div class="info-box-content">
                    <div class="info-box-text"><h4><?php echo $file['name']; ?></h4></div>
                    <div class="info-box-text"><?php echo $this->Form->postLink(
                        __('Delete') . ' <span class="sr-only">' . $file['name'] . '</span>',
                        '/files/delete/' . $file['id'],
                        array(
                            'confirm' => __('Are you sure?'),
                            'escape' => false
                        )
                    );?></div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>
