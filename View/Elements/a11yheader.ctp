<header class="main-header">
    <nav class="navbar navbar-static-top">
        <div class="container">
            <div class="navbar-header">
                <ul class="nav navbar-nav">
                    <li><?php echo $this->Html->link(
                        __('Jump to Content'),
                        '#main-content'
                    ); ?></li>
                    <li><?php echo $this->Html->link(
                        __('Jump to Navigation'),
                        '#main-nav'
                    ); ?></li>
                </ul>
            </div>
            <div class="navbar-custom-menu pull-right">
                <ul class="nav navbar-nav">
                    <li><?php echo $this->Html->link(
                            '<i class="fa fa-adjust"></i>' . __('Contrast'),
                            '#',
                            ['escape' => false, 'id' => 'contrast-btn', 'role' => 'button']);
                        ?></li>
                    <li><?php echo $this->Html->link(
                            '<i class="fa fa-wheelchair"></i>' . __('Accessibility'),
                            '/pages/accessibility',
                            ['escape' => false]);
                        ?></li>
                </ul>
            </div>
        </div>
    </nav>
</header>
