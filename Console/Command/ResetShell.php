<?php

class ResetShell extends AppShell
{
    public $uses = ['Atmodel', 'File', 'Category'];

    public function main()
    {
        echo 'Deletando Arquivos' . PHP_EOL;
        array_map('unlink', glob(WWW_ROOT . 'files/*.*'));
        echo 'Deletando Imagens' . PHP_EOL;
        array_map('unlink', glob(WWW_ROOT . 'img/uploads/*.*'));

        echo 'Deletando BD ...' . PHP_EOL;
        echo '... Arquivos' . PHP_EOL;
        $this->File->query('DELETE FROM files');
        echo '... Modelos de TA' . PHP_EOL;
        $this->Atmodel->query('DELETE FROM atmodels');
        echo '... Categorias' . PHP_EOL;
        $this->Category->query('DELETE FROM categories');
    }
}
